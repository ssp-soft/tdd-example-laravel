<?php

namespace App\Service;

use App\Model\Book;
use Exception;

use Illuminate\Support\Facades\Auth;
use function PHPUnit\Framework\isNull;

class BookService
{

    public function book($bookId)
    {
        $book = Book::where('id', $bookId)->firstOrFail();
        $user = Auth::user();

        if (!$user->vip && count($user->book()->get()) == 1) return false;
        if (!isNull($book->guest_id)) return false;

        $book->guest_id = $user->id;
        $book->save();

        return true;
    }
}
