<?php

namespace App\Http\Controllers;

use App\Model\Book;
use App\Service\BookService;
use Illuminate\Http\Request;

class BookController extends Controller {
    public function book(Book $book) {
        (new BookService)->book($book->id);
        return redirect()->route('home')->with('message', 'has been booked');
    }

    public function upload(Request $request) {

        $request->file('image')->store('public');

        return redirect()->route('home');
    }
}
