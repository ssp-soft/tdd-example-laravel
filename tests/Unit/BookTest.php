<?php

namespace Tests\Unit;

use App\Model\Book;
use App\Model\User;
use Tests\TestCase;
use App\Service\BookService;
use Illuminate\Foundation\Testing\RefreshDatabase;

class BookTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_a_non_vip_user_can_only_book_one()
    {
        $this->withExceptionHandling();
        $book1 = Book::create(['name' => 'book1']);
        $book2 = Book::create(['name' => 'book2']);

        $user = factory(User::class)->create(['vip' => false]);

        $this->actingAs($user);
        $result1 = (new BookService())->book($book1->id);

        $this->assertTrue($result1);

        $result2 = (new BookService())->book($book2->id);
        $this->assertFalse($result2);
        $this->assertEquals(1, count($user->book()->get()));
    }

    public function test_a_vip_user_can__book_many()
    {
        $this->withExceptionHandling();
        $book1 = Book::create(['name' => 'book1']);
        $book2 = Book::create(['name' => 'book2']);

        $user = factory(User::class)->create(['vip' => true]);

        $this->actingAs($user);
        $result1 = (new BookService())->book($book1->id);

        $this->assertTrue($result1);

        $result2 = (new BookService())->book($book2->id);
        $this->assertTrue($result2);
        $this->assertEquals(2, count($user->book()->get()));
    }
}
