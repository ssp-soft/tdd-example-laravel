<?php

namespace Tests\Feature;

use App\User;
use BookSeeder;
use Tests\TestCase;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class BookTest extends TestCase {
    use RefreshDatabase;

    public function test_a__book() {
        $this->seed(BookSeeder::class);

        $user = factory(User::class)->create(['vip' => false]);
        $response = $this->actingAs($user)->post('/book/1');

        $response->assertRedirect('/home');
        $response->assertSessionHas('message', 'has been booked');
    }

    public function test_a_not_book_because_it_doesnt_exist() {
        $user = factory(User::class)->create(['vip' => false]);
        $response = $this->actingAs($user)->post('/book/90');

        $response->assertNotFound();
    }

    public function test_only_loggedIn_user_can_book() {
        $this->seed(BookSeeder::class);

        $response = $this->post('/book/1');

        $response->assertRedirect('/login');
    }

    public function test_upload_foto() {
        $this->withoutExceptionHandling();
        $user = factory(User::class)->create(['vip' => false]);

        $pic = UploadedFile::fake()->image('image.png');
        $response = $this->actingAs($user)->post('/upload_picture', ['image' => $pic]);

        $response->assertRedirect('/home');
        Storage::disk('public')->assertExists($pic->hashName());
    }
}
