<?php

namespace Tests\Feature;

use Tests\TestCase;

use Illuminate\Foundation\Testing\RefreshDatabase;

class ViewTest extends TestCase {

    use RefreshDatabase;
    public function test_welcome_page() {
        $response = $this->get('/');

        $response->assertStatus(200)
            ->assertSeeText('Laravel')
            ->assertViewIs('welcome');
    }


}
